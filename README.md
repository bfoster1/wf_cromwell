# wf_cromwell

# Related Repositories:

wdl files, aws launch templates, aws scripts  
https://gitlab.com/bfoster1/wf_templates.git

Python wrapper scripts for running pre-processing, cromwell-assembly and post processing.  
https://gitlab.com/bfoster1/wf_cromwell.git

Python pre-processing, and reporting scripts:  
https://gitlab.com/bfoster1/jgi-meta.git

![alt text](img/flow.png "Title Text")  
```
usage: jgi_metaq.run_metag_pipeline.py [-h] (-i INFILE | -f FILES)
                                       (--local | --jaws | --aws)
                                       [-o OUTPUTDIR] [-w WDL] [--host HOST]
                                       [--sample SAMPLE] [--skip_finalize]
                                       [--setuponly] [--verbose]

runs pipeline

optional arguments:
  -h, --help            show this help message and exit
  -i INFILE, --infile INFILE
                        setup.json input file (from pre_meta)
  -f FILES, --files FILES
                        filtered fastq.gz file to operate on
  --local               run cromwell locally
  --jaws                run cromwell with jaws
  --aws                 run cromwell at aws
  -o OUTPUTDIR, --outputdir OUTPUTDIR
                        output dir. default = cwd
  -w WDL, --wdl WDL     manually specify wdl instead of kmer autoselect
  --host HOST           cromwell host ip default=54.212.246.70:8000
  --sample SAMPLE       number of subsampled reads to run (for debugging)
  --skip_finalize       skip finalize step. Just retrieve results
  --setuponly           setup only, dont run cromwell
  --verbose             run cromwell file locally debug
```
