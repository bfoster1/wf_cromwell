#!/usr/bin/env python

import sys, os
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","lib"))
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","..","jgi-meta","lib"))
import report_utils as utils
import argparse
import json
import subprocess
import shutil
import re
import time
import hashlib
import glob

#requires an active aws cli if using s3

if sys.version_info[0] <3:
    sys.stderr.write('use python3' + "\n")
    sys.exit(1)
if shutil.which('aws') is None:
    sys.stderr.write('awscli not in path or not installed' + "\n")
    sys.exit(1)

# local and remote



VERSION = "1.0.0"
def main():
    '''given a reads inputfile, and a wdl, copy reads to s3 and launch cromwell job 
    wait for completion and copy outputs back
    '''
    
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-i", "--inputfile", required=True, help="input file.\n")
 
    parser.add_argument("-w", "--wdl", required=True, help="wdl file for cromwell.\n")
    parser.add_argument("-j", "--jarfile", required=False, help="cromwell jarfile .\n")        
    parser.add_argument("-o", "--outputdir", required=False, default=os.getcwd(), help="outputdir for outputs.\n")
    parser.add_argument("-f", "--outputfile", required=False,default="metadata_cromwell.json", help ="file name to file with metadatapath ")
    parser.add_argument("-t", "--time", required=False, default=30, help="time in seconds to monitor the directory. (default=60)\n")

    parser.add_argument("--verbose", required=False, default=False,action='store_true', help="verbose mode")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    args = parser.parse_args()

    if args.outputdir and not os.path.exists(args.outputdir):
        sys.stderr.write(args.outputdir + " does not exist \n")
        sys.exit(255)
    if not os.path.exists(args.wdl):
        sys.stderr.write(args.wdl + " does not exist \n")
        sys.exit(255)        

    jarpath=""
    if args.jarfile:
        if os.path.exists(args.jarfile):
            jarpath = args.jarfile
        else:
            sys.stderr.write("jarfile " + jarfile + " not found")
            sys.exit()            
    else:
        try:
            jarpath = glob.glob(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","..","jgi-meta","bin","cromwell-*.jar"))[0]
        except:
            sys.stderr.write("jarfile not found")
            sys.exit()


    #get workflow name frow wdl
    workflow = ""
    with open(args.wdl,"r") as f:
        for line in f.readlines():
            if re.match(r'^\s*workflow', line) is not None:
                workflow = line.split()[1]
                workflow = re.sub(r"{",r'',workflow)
                workflow = re.sub(r'\s',r'',workflow)


    #create the cromwell inputs file
    ts = str(time.time())
    final_inputs_json = os.path.join(args.outputdir, ts + ".inputs.json")
    inputs_json = dict()    
    inputs_json[workflow + ".input_file"] = args.inputfile
    inputs_json["run_dir"] = args.outputdir
    inputs_json["wdl"] = args.wdl
    inputs_json["setup_json"] = ""
    inputs_json["prefix"] = ts
    inputs_json["metadata"] = {"input_files":args.inputfile}
    with open(final_inputs_json,"w") as f:f.write(json.dumps(inputs_json,indent=3))

    metadata_cromwell =  final_inputs_json + ".metadata_cromwell.json"
    cmd = 'java -jar ' + jarpath + ' run -i ' + final_inputs_json + ' -m ' + metadata_cromwell +  " "  + args.wdl
    result = utils.run_process(cmd,verbose=args.verbose)

    if args.outputfile:
        output_full_path = os.path.join(args.outputdir, os.path.basename(args.outputfile))
        with open(output_full_path, "w") as f:
            f.write(json.dumps({"metadata_cromwell":metadata_cromwell},indent=3))

if __name__ == '__main__':
    main()

