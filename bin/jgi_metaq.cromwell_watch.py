#!/usr/bin/env python

import sys, os
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","lib"))
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","..","jgi-meta","lib"))
import report_utils as utils
import argparse
import json
import subprocess
import shutil
import re
import time
import hashlib


#requires an active aws cli if using s3

if sys.version_info[0] <3:
    sys.stderr.write('use python3' + "\n")
    sys.exit(1)
if shutil.which('aws') is None:
    sys.stderr.write('awscli not in path or not installed' + "\n")
    sys.exit(1)

# local and remote


VERSION = "1.0.0"
def main():
    '''given a reads inputfile, and a wdl, copy reads to s3 and launch cromwell job 
    wait for completion and copy outputs back
    '''

    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-i", "--inputfile", required=True, help="input file.\n")
    parser.add_argument("-o", "--outputdir", required=False, default=os.getcwd(), help="outputdir for outputs. default=send to cromwell only\n")
    parser.add_argument("--outputfile", required=False, help="outputdir for outputs. default=send to cromwell only\n")
    parser.add_argument("-w", "--wdl", required=True, help="wdl file for cromwell.\n")
    parser.add_argument("-s", "--s3", required=False,default='s3://bf-20190529-uswest2-s3/cromwell-execution', help="s3 destination for input file/n")
    parser.add_argument("-c", "--cromwellhost", required=False,default="54.212.246.70:8000",  help="cromwell host to push job to\n")

    parser.add_argument("-t", "--time", required=False, default=30, help="time in seconds to monitor the directory. (default=60)\n")

    parser.add_argument("--verbose", required=False, default=False,action='store_true', help="verbose mode")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    args = parser.parse_args()

    if not args.s3.startswith('s3://'):
        sys.stderr.write(args.s3 + " does not look like s3://\n")
        sys.exit(1)
    if args.outputdir and not os.path.exists(args.outputdir):
        sys.stderr.write(args.outputdir + " does not exist \n")
        sys.exit(255)
    if not os.path.exists(args.wdl):
        sys.stderr.write(args.wdl + " does not exist \n")
        sys.exit(255)        
        
    args.outputdir == os.path.abspath(args.outputdir)

    cmd = ['curl', '-Is', '--max-time', '10', 'http://' + args.cromwellhost ]
    result = utils.run_process(cmd,verbose=False)

    #get workflow name frow wdl
    workflow = ""
    with open(args.wdl,"r") as f:
        for line in f.readlines():
            if re.match(r'^\s*workflow', line) is not None:
                workflow = line.split()[1]
                workflow = re.sub(r"{",r'',workflow)
                workflow = re.sub(r'\s',r'',workflow)

    if not args.inputfile.startswith("s3://"):
        inputs_file_s3 = os.path.join(args.s3,os.path.basename(args.inputfile))
        cmd = ["aws","s3", "cp", args.inputfile, inputs_file_s3]
        result = utils.run_process(cmd,verbose=args.verbose)


    #create the cromwell inputs file
    ts = str(time.time())
    final_inputs_json = os.path.join(args.outputdir, ts + ".inputs.json")
    inputs_json = dict()    
    inputs_json[workflow + ".input_file"] = inputs_file_s3
#    inputs_json[workflow + ".uniquekmer"] = kmers
    inputs_json["run_dir"] = args.outputdir
    inputs_json["wdl"] = args.wdl
    inputs_json["setup_json"] = ""
    inputs_json["prefix"] = ts
    inputs_json["metadata"] = {"input_files":args.inputfile}
    with open(final_inputs_json,"w") as f:f.write(json.dumps(inputs_json,indent=3))

    cmd = 'curl --silent -X POST "http://' + args.cromwellhost + '/api/workflows/v1" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" '
    cmd += '-F "workflowSource=@' + args.wdl + ';" ' 
    cmd += '-F "workflowInputs=@' + final_inputs_json + ';type=application/json" '
    #    cmd += '-F "workflowOptions=@options.json;type=application/json" '
    result = utils.run_process(cmd,verbose=args.verbose)
    submission = json.loads(result['stdout'])
    sub_id = submission['id']
    if args.verbose: sys.stdout.write("cromwell_id: " + sub_id + "\n");sys.stdout.flush()    

    cmd = 'curl --silent -X GET "http://' + args.cromwellhost + '/api/workflows/v1/' + sub_id + '/metadata?expandSubWorkflows=false" -H  "accept: application/json"'
    if args.verbose: sys.stdout.write("cromwell_check: " + cmd + "\n");sys.stdout.flush()
    key = args.cromwellhost + "_" + sub_id
    logfile = os.path.join(args.outputdir, ts + ".cromwell_status.log")
    ### Start submission loop
    finished = False
    while not finished:
        time.sleep(int(args.time))        
        result = utils.run_process(cmd,verbose=False)
        metadata = json.loads(result['stdout'])
        if args.verbose: sys.stdout.write("cromwell_status: " + metadata['status'] + "\n");sys.stdout.flush()    
        with open(logfile,"a") as f:
            f.write(time.strftime("%Y-%m-%d %H:%M:%S\t") + key + "\t" + metadata['status'] + "\n")
        #['Running', 'Succeeded', 'Aborted', 'Submitted', 'On Hold', 'Failed']
        if metadata['status'] not in ['Submitted', 'Running', 'On Hold']:
            finished = True

    if metadata['status'] in ['Aborted', 'Failed']:
        sys.stderr.write("Error, check on: " + metadata['id'] + "\n")
        sys.exit(1)

    if args.outputfile:
        metadata_cromwell =  os.path.join(args.outputdir, ts + ".metadata_cromwell.json")
        with open(metadata_cromwell,"w") as f:f.write(json.dumps(metadata,indent=3))
        output_full_path = os.path.join(args.outputdir, os.path.basename(args.outputfile))
        with open(output_full_path, "w") as f:
            f.write(json.dumps({"metadata_cromwell":metadata_cromwell},indent=3))

    sys.exit(0)
        


if __name__ == '__main__':
    main()

