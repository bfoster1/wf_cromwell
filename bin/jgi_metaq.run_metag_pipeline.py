#!/usr/bin/env python

import sys, os
wf_cromwell_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","lib")
wf_template_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","..","wf_templates","templates")
jgi_meta_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","..","jgi-meta","lib")
sys.path.insert(0,wf_cromwell_path)
sys.path.insert(0,jgi_meta_path)

import subprocess
import json 
import argparse
import time
import report_utils as utils
import shutil


scripts=dict()
scripts['pre']="/global/projectb/sandbox/gaag/bfoster/eclipse2/repos/jgi-meta/bin/pre_meta_assembly.py"
scripts['finalize']="/global/projectb/sandbox/gaag/bfoster/eclipse2/repos/jgi-meta/bin/cromwell_finalize.py"
scripts['file_prep']="/global/projectb/sandbox/gaag/bfoster/eclipse2/repos/wf_cromwell/bin/jgi_metaq.file_prep.py"
scripts['cromwell_local']="/global/projectb/sandbox/gaag/bfoster/eclipse2/repos/wf_cromwell/bin/jgi_metaq.cromwell_local.py"
scripts['cromwell_watch']="/global/projectb/sandbox/gaag/bfoster/eclipse2/repos/wf_cromwell/bin/jgi_metaq.cromwell_watch.py"
scripts['cromwell_retrieve']="/global/projectb/sandbox/gaag/bfoster/eclipse2/repos/wf_cromwell/bin/jgi_metaq.cromwell_retrieve.py"


def main():
    '''runs pipeline'''
    parser = argparse.ArgumentParser(description=main.__doc__)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-i", "--infile", required=False, help="setup.json input file (from pre_meta)")
    group.add_argument("-f", "--files", required=False, help="filtered fastq.gz file to operate on")
    group.add_argument("-u", "--userfile", required=False, help="user provided file (Non-rqc filtered)")

    group2 = parser.add_mutually_exclusive_group(required=True)
    group2.add_argument("--local", required=False,default=False, action='store_true', help="run cromwell locally\n")
    group2.add_argument("--jaws", required=False,default=False, action='store_true', help="run cromwell with jaws\n")
    group2.add_argument("--aws", required=False,default=False, action='store_true', help="run cromwell at aws\n")

    parser.add_argument("-o", "--outputdir", required=False, default=os.getcwd(), help="output dir. default = cwd\n")
    parser.add_argument("-w", "--wdl", required=False, help="manually specify wdl instead of kmer autoselect\n")
    parser.add_argument("--host", required=False,default='54.212.246.70:8000', help="cromwell host ip default=54.212.246.70:8000")
    parser.add_argument("--sample", required=False,default=0, help="number of subsampled reads to run (for debugging)\n")
    parser.add_argument("--skip_finalize", required=False,default=False, action='store_true', help="skip finalize step. Just retrieve results\n")
    parser.add_argument("--setuponly", required=False,default=False, action='store_true', help="setup only, dont run cromwell\n")
    parser.add_argument("--verbose", required=False,default=False, action='store_true', help="run cromwell file locally debug \n")
    args = parser.parse_args()

    if not os.path.exists(args.outputdir):
        os.mkdir(args.outputdir)

    #1) using filtered file get setup.json
    #2) run file prep
    #3) pick wdl based on kmer and local
    #4) launch local or remote script
    #5) retreive cromwell
    #6) finalize

    if args.jaws:
        sys.stderr.write("Jaws not yet implemented.\n")
        sys.exit(1)
    setup_file = os.path.join(args.outputdir,"setup.json")            
    if args.infile:
        setup_file = args.infile
        if not os.path.exists(args.infile):
            sys.stderr.write(args.infile + " doesn't exist\n")
            sys.exit(255)
    elif args.userfile:
        args.skip_finalize=True
        if not args.userfile.endswith(".gz"):
            sys.stderr.write("input should be gzipped")
            sys.exit(1)
        if not os.path.exists(args.userfile):
            sys.stderr.write(args.userfile + " doesn't exist\n")
            sys.exit(255)
        with open(setup_file,"w") as f:
            f.write('{"inputs":["' + args.userfile + '"]}' + "\n")
    else:
        cmd = scripts['pre'] + ' ' + args.files + ' -o ' + setup_file
#        cmd = wrap_if_exvivo(cmd,args.outputdir)
        utils.run_process(cmd,verbose=args.verbose,fail=True)


    original_setup_file = setup_file
    if args.sample:
        original_setup_file = debug_setup(setup_file,args.outputdir,verbose=args.verbose, sample=int(args.sample))

    #$META/../../wf_cromwell/bin/jgi_metaq.file_prep.py -i $PWD/setup.json -o $PWD --json $PWD/input_files.json
    input_files_json = os.path.join(args.outputdir,"input_files.json")
    cmd = scripts['file_prep'] + " -i " + setup_file  + " -o " + args.outputdir + " --json " +  input_files_json
    utils.run_process(cmd,verbose=args.verbose,fail=True)

    #get kmer file and choose wdl
    with open(input_files_json,"r") as f:in_json=json.loads(f.read())
    with open(in_json['kmerfile'],"r") as f:kmers = int(f.read().rstrip())

    assy_size=""
    if kmers<5000000000:
        assy_size="small"
    elif kmers<15000000000:
        assy_size="med"
    elif kmers<30000000000:
        assy_size="large"
    elif args.wdl:
        assy_size="NA"
    elif args.local:
        assy_size="NA"        
    else:
        sys.stderr.write("assy too large")
        sys.exit()

    wdl_path = ""
    if args.wdl and os.path.exists(args.wdl): #specified path
        wdl_path = args.wdl
    elif args.wdl: #look for path in templates dir
        wdl_path = os.path.join(wf_template_path,os.path.basename(args.wdl))
    elif args.local:
        wdl_path = os.path.join(wf_template_path,"jgi_meta.local.120.480.120.wdl")
    elif assy_size=="small":
        wdl_path = os.path.join(wf_template_path,"jgi_meta.aws.120.120.120.wdl")
    elif assy_size=="med":
        wdl_path = os.path.join(wf_template_path,"jgi_meta.aws.120.240.120.wdl")
    elif assy_size=="large":
        wdl_path = os.path.join(wf_template_path,"jgi_meta.aws.120.480.120.wdl")

    if not os.path.exists(wdl_path):
        sys.sderr.write("cant find " + wdl_path)
        sys.exit()        

    metadata_fof = os.path.join(args.outputdir,"metadata_cromwell.json")
    if args.local:
        #$META/../../wf_cromwell/bin/jgi_metaq.cromwell_local.py -i *gz -w $META/../../wf_templates/templates/jgi_meta.aws.120.480.120.local_debug.wdl
        cmd = scripts['cromwell_local'] + " -i " +  in_json['infile']  + " -w " + wdl_path + " -o " + args.outputdir  + " --outputfile " + os.path.basename(metadata_fof)
    else:
        cmd = scripts['cromwell_watch'] + " -i " +  in_json['infile']  + " -w " + wdl_path + " -o " + args.outputdir  + " --outputfile " + os.path.basename(metadata_fof) + " --cromwellhost " + args.host
    if args.setuponly:
        print(cmd);sys.exit()
    else:
        utils.run_process(cmd,verbose=args.verbose,fail=True)
    cmd = scripts['cromwell_retrieve'] + " -m " + "$(jq -r .metadata_cromwell " + metadata_fof + ") -o " + args.outputdir
    utils.run_process(cmd,verbose=args.verbose,fail=True)

    #/global/projectb/sandbox/gaag/bfoster/eclipse2/repos/jgi-meta/bin/cromwell_finalize.py -i setup.json.1573687906.3220158 -o $PWD/1573687907.9178035.699a1a38-664b-4a29-92e2-fe4a3dbe7ce0/
    cmd = scripts['finalize']  + " -o $(cat " + os.path.join(args.outputdir,"dirname.txt") + ") -i " + original_setup_file

    if args.local:
        cmd = cmd + " --cromwell_local"
#    cmd=wrap_if_exvivo(cmd,args.outputdir)
    if not args.skip_finalize:
        result = utils.run_process(cmd,verbose=args.verbose,fail=True)
        with open(os.path.join(args.outputdir,"status.log"),"w") as f:
            f.write(result['stdout'])
    
def debug_setup(json_file,outputdir,verbose, sample):
    '''subsample 400000 reads and modify json'''
    with open(json_file,"r") as f:json_meta=json.loads(f.read())
    tmpfile = os.path.join(outputdir, str(time.time()) + ".fastq.gz")
    #cmd = "shifter --image=bryce911/bbtools:latest -- reformat.sh in=" + json_meta['inputs'][0] + " reads=50000 out=" + tmpfile
    cmd = "zcat " + json_meta['inputs'][0] + " | head -" + str(sample * 4) + " | gzip -n  > " + tmpfile
    utils.run_process(cmd,verbose=verbose,fail=True)
    cmd = "md5sum " + tmpfile + " > " + tmpfile + ".md5"
    utils.run_process(cmd,verbose=verbose,fail=True)
    with open( tmpfile + ".md5", "r") as f:md5=f.read().split()[0].rstrip()
    sampled = os.path.join(outputdir, md5 + ".samp." + os.path.basename(json_meta['inputs'][0]))
    cmd = "mv " + tmpfile + " " + sampled
    utils.run_process(cmd,verbose=verbose,fail=True)

    json_meta['inputs']= [sampled]
    orig_file = json_file + "." + str(time.time())
    cmd = "mv " + json_file + " " + orig_file
    utils.run_process(cmd,verbose=verbose,fail=True)
    with open(json_file,"w") as f:f.write(json.dumps(json_meta,indent=3))
    return orig_file

def wrap_if_exvivo(cmdstring,dirname):
    '''test env var for exvivo'''
    exvivo = False
    if 'SLURMD_NODENAME' in os.environ and os.environ['SLURMD_NODENAME'].startswith("exvivo"):
        exvivo=True
    if exvivo:
        cmdstring = 'ssh cori "cd ' +  dirname + ' && source /global/projectb/sandbox/gaag/bfoster/eclipse2/miniconda3/etc/profile.d/conda.sh && conda activate && ' + cmdstring + '"'
    return cmdstring

if __name__=="__main__":
    main()

